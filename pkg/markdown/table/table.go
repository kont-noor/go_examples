package table

import (
	"encoding/csv"
	"fmt"
	"log"
	"strings"
)

func Draw(source string) string {
	var result strings.Builder
	r := csv.NewReader(strings.NewReader(source))

	lines, err := r.ReadAll()

	if err != nil {
		log.Fatal(err)
	}

	widths := make([]int, len(lines[0]))

	// calculate max length
	for _, row := range lines {
		for i, col := range row {
			current_length := len([]rune(col))
			if current_length > widths[i] {
				widths[i] = current_length
			}
		}
	}

	//print
	for i, row := range lines {
		for j, col := range row {
			spaces := strings.Repeat(" ", widths[j]-len([]rune(col))+1)
			fmt.Fprint(&result, "| ", col, spaces)
		}
		fmt.Fprintln(&result, "|")
		if i == 0 {
			for _, width := range widths {
				dashes := strings.Repeat("-", width)
				fmt.Fprint(&result, "| ", dashes, " ")
			}
			fmt.Fprintln(&result, "|")
		}
	}
	return result.String()
}
