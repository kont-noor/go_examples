package table_test

import (
	"github.com/stretchr/testify/assert"
	"start_tour/pkg/markdown/table"
	"strings"
	"testing"
)

func TestFunctionality(t *testing.T) {
	assert := assert.New(t)

	result := `
| head1  | head2  | head3  | head4  | head5      |
| ------ | ------ | ------ | ------ | ---------- |
| value1 |        | value3 | value4 |            |
|        |        |        |        |            |
|        | value2 | value3 |        | value5     |
| value1 | value2 | value3 | value4 | value5last |
`
	csv := `head1,head2,head3,head4,head5
value1,,value3,value4,
,,,,
,value2,value3,,value5
value1,value2,value3,value4,value5last`

	assert.Equal(strings.TrimLeft(result, " \n"), table.Draw(csv), "they should be equal")
}
