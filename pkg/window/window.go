package window

import (
	"fmt"
	"math"
	"strings"
)

type Window struct {
	Title string
	Body  string
	Width int
}

func Draw(params Window) string {
	var result strings.Builder
	drawSeparator(params.Width, &result)
	drawTitle(params.Title, params.Width, &result)
	drawSeparator(params.Width, &result)
	drawBody(params.Body, params.Width, &result)
	drawSeparator(params.Width, &result)
	return result.String()
}

func drawTitle(text string, width int, buffer *strings.Builder) {
	whitespace_length := width - len([]rune(text)) - 6
	left_indent := whitespace_length / 2
	right_indent := left_indent + int(math.Mod(float64(whitespace_length), 2))
	fmt.Fprintln(buffer, "|", strings.Repeat(" ", left_indent), text, strings.Repeat(" ", right_indent), "|")
}

func drawBody(text string, width int, buffer *strings.Builder) {
	lines := strings.Split(text, "\n")

	for _, line := range lines {
		drawLine(line, width, buffer)
	}
}

func drawLine(text string, width int, buffer *strings.Builder) {
	right_indent := width - len([]rune(text)) - 5
	fmt.Fprintln(buffer, "|", text, strings.Repeat(" ", right_indent), "|")
}

func drawSeparator(width int, buffer *strings.Builder) {
	fmt.Fprintln(buffer, strings.Repeat("-", width))
}
