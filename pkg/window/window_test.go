package window_test

import (
	"github.com/stretchr/testify/assert"
	"start_tour/pkg/window"
	"strings"
	"testing"
)

func TestFunctionality(t *testing.T) {
	assert := assert.New(t)

	result := `
--------------------
|    Test title    |
--------------------
| Test body        |
| test             |
| line line line   |
--------------------
`

	params := window.Window{Title: "Test title", Body: "Test body\ntest\nline line line", Width: 20}

	assert.Equal(strings.TrimLeft(result, " \n"), window.Draw(params), "they should be equal")
}
