package bot

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"log"
	"os"
	"start_tour/pkg/calendar"
	"strings"
)

//type UserConfig struct {
//  telegram_id string
//  calendar_token string
//}

var userTokens map[int]string

//var UserTokens [5]UserConfig

var keyboard = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("/authorize"),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("/schedule"),
		tgbotapi.NewKeyboardButton("/orders"),
		tgbotapi.NewKeyboardButton("/results"),
	),
)

var state string

func Start() {
	userTokens = make(map[int]string)
	bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_TOKEN"))
	if err != nil {
		panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil { // ignore any non-Message Updates
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		if update.Message.IsCommand() {
			handleCommand(bot, update)
		} else {
			handleText(bot, update)
		}
	}
}

func handleCommand(bot *tgbotapi.BotAPI, update tgbotapi.Update) {
	switch update.Message.Command() {
	case "authorize":
		link := calendar.GetOauthLink()
		text := "Follow this link in browser:\n" + link + "\nThen paste the authorization code here"
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, text)
		msg.ReplyMarkup = keyboard
		bot.Send(msg)
		state = "token"
	case "schedule":
		token, ok := userTokens[update.Message.From.ID]
		if ok {
			events := calendar.GetSchedule(token)
			var text string
			if len(events) == 0 {
				text = "no events"
			} else {
				text = "your events:\n" + strings.Join(events[:], "\n")
			}
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, text)
			msg.ReplyMarkup = keyboard
			bot.Send(msg)
		} else {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "not authorized")
			msg.ReplyMarkup = keyboard
			bot.Send(msg)
		}
	default:
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "unknown command: "+update.Message.Command())
		msg.ReplyMarkup = keyboard
		bot.Send(msg)
	}
}

func handleText(bot *tgbotapi.BotAPI, update tgbotapi.Update) {
	switch state {
	case "token":
		token := calendar.GetTokenString(update.Message.Text)
		userTokens[update.Message.From.ID] = token

		msg_template := "authorized with token for user "
		msg_string := fmt.Sprintf("%s%d", msg_template, update.Message.Chat.ID)
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, msg_string)
		bot.Send(msg)
		state = ""
	default:
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "привет, "+update.Message.From.FirstName)
		bot.Send(msg)

		msg = tgbotapi.NewMessage(update.Message.Chat.ID, "Абсолютно с тобой согласен, "+update.Message.From.FirstName)
		msg.ReplyToMessageID = update.Message.MessageID

		bot.Send(msg)
	}
}
