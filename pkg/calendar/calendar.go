package calendar

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/calendar/v3"
	"google.golang.org/api/option"
)

func initClient(config *oauth2.Config, token string) *http.Client {
	tok := &oauth2.Token{}
	json.Unmarshal([]byte(token), tok)
	return config.Client(context.Background(), tok)
}

func GetTokenString(authCode string) string {
	config := getConfig()
	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	token_string, _ := json.Marshal(tok)
	return string(token_string)
}

func GetOauthLink() string {
	config := getConfig()
	return config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
}

func getConfig() *oauth2.Config {
	b, err := ioutil.ReadFile("credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	// If modifying these scopes, delete your previously saved token.json.
	config, err := google.ConfigFromJSON(b, calendar.CalendarReadonlyScope)
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	return config
}

func GetSchedule(token string) []string {
	ctx := context.Background()
	config := getConfig()
	client := initClient(config, token)

	srv, err := calendar.NewService(ctx, option.WithHTTPClient(client))
	if err != nil {
		log.Fatalf("Unable to retrieve Calendar client: %v", err)
	}

	t := time.Now().Format(time.RFC3339)
	events, err := srv.Events.List("primary").ShowDeleted(false).
		SingleEvents(true).TimeMin(t).MaxResults(10).OrderBy("startTime").Do()
	if err != nil {
		log.Fatalf("Unable to retrieve next ten of the user's events: %v", err)
	}

	eventsArray := make([]string, len(events.Items))

	for i, item := range events.Items {
		date := item.Start.DateTime
		if date == "" {
			date = item.Start.Date
		}
		eventsArray[i] = fmt.Sprintf("%v (%v)\n", item.Summary, date)
	}

	return eventsArray
}
