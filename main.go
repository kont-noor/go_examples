package main

import (
	"fmt"
	"start_tour/pkg/bot"
	"start_tour/pkg/markdown/table"
	"start_tour/pkg/window"
)

func main() {
	params := window.Window{Title: "Test title", Body: "Test body\ntest\nline line line", Width: 20}
	fmt.Print(window.Draw(params))

	markdown := table.Draw(`head1,head2,head3,head4,head5
value1,,value3,value4,
,value2,value3,,value5
value1,value2,value3,value4,value5last`)
	fmt.Println(markdown)

	bot.Start()
}
